# Foundry Virtual Tabletop - DnD5e (+ Epic Characters) Game System

This game system for [Foundry Virtual Tabletop](http://foundryvtt.com) provides character sheet and game system 
support for the Fifth Edition of the world's most popular roleplaying game.

This system is offered and may be used under the terms of the Open Gaming License v1.0a and its accompanying
[Systems Reference Document 5.1 (SRD5)](http://media.wizards.com/2016/downloads/DND/SRD-OGL_V5.1.pdf).

This system provides character sheet support for Actors and Items, mechanical support for dice and rules necessary to
play games of 5th Edition, and compendium content for Monsters, Heroes, Items, Spells, Class Features, Monster 
Features, and more!

**This fork contains additional modifications to the system in order to provide built-in support for the [Epic Characters rules](https://www.dmsguild.com/product/173822/Epic-Characters), written by Mark Altfuldisch and published by the Dungeon Masters Guild. The development of this framework is made with the author's blessing. These modifications merely exist to support the content as a technical framework (such as including higher-level spell slots and refactoring character levelling and proficiency bonuses), but do not contain any of the content itself (such as spells or class features); it is the responsibility of the user to purchase the rules separately to use the Epic Characters content in tandem with this system.**

The software component of this system is distributed under the GNUv3 license.

## Installation Instructions

To install and use the DnD5e system for Foundry Virtual Tabletop, simply paste the following URL into the 
**Install System** dialog on the Setup menu of the application.

https://gitlab.com/foundrynet/dnd5e/raw/master/system.json

If you wish to manually install the system, you must clone or extract it into the ``Data/systems/dnd5e`` folder. You
may do this by cloning the repository or downloading a zip archive from the
[Releases Page](https://gitlab.com/foundrynet/dnd5e/-/releases).

## Community Contribution

Code and content contributions are accepted. Please feel free to submit issues to the issue tracker or submit merge
requests for code changes. Approval for such requests involves code and (if necessary) design review by Atropos. Please
reach out on the Foundry Community Discord with any questions.
